import comprasonline.SQLConnection;


/**
 * @author Ramiro
 */
public class Verificacion_sesion extends javax.swing.JFrame {

    public Verificacion_sesion() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        join = new javax.swing.JButton();
        signup = new javax.swing.JButton();
        cont_usuario = new javax.swing.JTextField();
        cont_clave = new javax.swing.JPasswordField();
        username = new javax.swing.JLabel();
        password = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(51, 51, 255));

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        jLabel1.setText("Iniciar sesión");

        join.setFont(new java.awt.Font("Arial Narrow", 2, 18)); // NOI18N
        join.setForeground(new java.awt.Color(0, 0, 153));
        join.setText("Ingresar");
        join.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                joinActionPerformed(evt);
            }
        });

        signup.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        signup.setForeground(new java.awt.Color(0, 0, 153));
        signup.setText("Registrase");
        signup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signupActionPerformed(evt);
            }
        });

        cont_usuario.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N

        cont_clave.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        cont_clave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cont_claveActionPerformed(evt);
            }
        });

        username.setFont(new java.awt.Font("Calibri", 2, 24)); // NOI18N
        username.setText("Usuario");

        password.setFont(new java.awt.Font("Calibri", 2, 24)); // NOI18N
        password.setText("Clave");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(username)
                            .addComponent(password))
                        .addGap(33, 33, 33)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cont_usuario, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cont_clave, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(132, 132, 132)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(94, 94, 94)
                        .addComponent(join)
                        .addGap(34, 34, 34)
                        .addComponent(signup)))
                .addContainerGap(63, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel1)
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(username)
                            .addComponent(cont_usuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(cont_clave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4))
                    .addComponent(password))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(join)
                    .addComponent(signup))
                .addContainerGap(69, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void joinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_joinActionPerformed

        SQLConnection con = new SQLConnection();
        con.conexion();

        if(con.Logueo(cont_usuario.getText(), cont_clave.getText()) == 1){
            System.out.println("ok");
        } else {
            System.out.println("error");
        }  

    }//GEN-LAST:event_joinActionPerformed

    private void cont_claveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cont_claveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cont_claveActionPerformed

    private void signupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signupActionPerformed
        Registro_sesion f1 = new Registro_sesion();
        f1.setVisible(true);
        dispose();
    }//GEN-LAST:event_signupActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(() -> {
            V.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField cont_clave;
    private javax.swing.JTextField cont_usuario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton join;
    private javax.swing.JLabel password;
    private javax.swing.JButton signup;
    private javax.swing.JLabel username;
    // End of variables declaration//GEN-END:variables
    
    public static Verificacion_sesion V = new Verificacion_sesion();
}
