package comprasonline;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 * @author Ramiro
 */

public class SQLConnection {
    Connection con;
    public void conexion(){
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String URL = "jdbc:sqlserver://DESKTOP-4MG3A9U\\SQLEXPRESS:1433;databaseName=compraonline;user=admin;password=123";
            con = DriverManager.getConnection(URL);
            System.out.println("Conexion establecida");
        
        }
        catch (ClassNotFoundException | SQLException e){
            System.err.println("Error: "+e);
        }
    }
    
    public int Logueo(String Usuario, String Clave){
        Integer resultado = 0;
        try{
            Statement ejecutor = con.createStatement();
            String sql1 = "SELECT * FROM usuarios WHERE Usuario='"+Usuario+"' and Clave='"+Clave+"'";
            ResultSet rs = ejecutor.executeQuery(sql1);
            
            if(rs.next()){
                JOptionPane.showMessageDialog(null, "Inicio de sesion exitoso");
                resultado = 1;
            } else {
                JOptionPane.showMessageDialog(null, "Contraseña o Usuario incorrecto \n Reintentelo... ");
                resultado = 0;
            }
        }
        catch (SQLException e){
            System.err.println("Error: "+e);
        }
        
        return resultado;
    }
    
    public int Registro(String Nombre, String Apellido, String Usuario, String Direccion, String Clave){
        Integer res = 0;
        try {
            Statement ejecut = con.createStatement();
            String sql2 = "INSERT INTO usuarios (Nombre, Apellido, Usuario, Direccion, Clave) VALUES ('"+Nombre+"', '"+Apellido+"', '"+Usuario+"', '"+Direccion+"', '"+Clave+"')";
            int rs = ejecut.executeUpdate(sql2);
            
            if(rs != 0){
                JOptionPane.showMessageDialog(null, "Registro concretado \n Redirigiendo...");
                res = 1;
            } else {
                JOptionPane.showMessageDialog(null, "Se ha detectado un error");
                res = 0;
            }
        }
        catch (SQLException e){
            System.err.println("Error: "+e);
        }
        return res;
    }
}

